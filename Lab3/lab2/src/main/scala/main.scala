import scalafx.application.JFXApp
import scalafx.geometry.Pos
import scalafx.scene.Scene
import scalafx.scene.control.{Button, Label, RadioButton, TextArea, TextField, ToggleGroup}
import scalafx.scene.layout.{BorderPane, VBox}
object main extends JFXApp {

  var tempType = 1//1=>Celsius, 2=>Fahrenheit
  stage = new JFXApp.PrimaryStage{
    title = "Temperature converter"
    width = 400
    height = 300
    resizable = false
    scene = new Scene {
      root = {
        val centerBox = new VBox {
          spacing = 20
          alignment = Pos.Center
          //textInputTemperature
          val inputTemperature = new TextField
          inputTemperature.setMaxWidth(80)
          //radioButtionToggleGroup
          val tog = new ToggleGroup()
          //radioButtonCelsius
          val radioButtonCelsius = new RadioButton
          radioButtonCelsius.setPrefWidth(120)
          radioButtonCelsius.setMaxHeight(50)
          radioButtonCelsius.setText("Celsius [°C]")
          radioButtonCelsius.setToggleGroup(tog)
          radioButtonCelsius.setSelected(true)
          //radioButtonHa
          val radioButtonFahrenheit = new RadioButton
          radioButtonFahrenheit.setPrefWidth(120)
          radioButtonFahrenheit.setMaxHeight(50)
          radioButtonFahrenheit.setText("Fahrenheit [°F]")
          radioButtonFahrenheit.setToggleGroup(tog)
          //Convert Button
          val applyButton = new Button
          applyButton.setPrefWidth(60)
          applyButton.setPrefHeight(40)
          applyButton.setText("Calculate")
          //converted temperature
          val convertedTemperature = new TextField
          convertedTemperature.setMaxWidth(160)
          convertedTemperature.setEditable(false)
          convertedTemperature.setText("Fahrenheit [°F] = ")

          children = List(Label("Choose input temperature type: "),
            radioButtonCelsius,
            radioButtonFahrenheit,
            inputTemperature,
            convertedTemperature)

          inputTemperature.text.onChange{
            var inputTemp = (inputTemperature.getText).toFloat
            var temp:Float = 0
            if(tempType ==1)
              {
                temp = (9/5.toFloat) * inputTemp + 32
                convertedTemperature.setText("Fahrenheit [°F] = " + f"$temp%1.1f")
              }
            else
            {
              temp = (inputTemp-32)/(9/5.toFloat)
              convertedTemperature.setText("Celsius [°C] = " + f"$temp%1.1f")
            }
          }

          radioButtonCelsius.selected.onChange{
            tempType = 1
            var inputTemp = (inputTemperature.getText).toFloat
            var temp:Float = 0
            if(tempType ==1)
            {
              temp = (9/5.toFloat) * inputTemp + 32
              convertedTemperature.setText("Fahrenheit [°F] = " + f"$temp%1.1f")
            }
            else
            {
              temp = (inputTemp-32)/(9/5.toFloat)
              convertedTemperature.setText("Celsius [°C] = " + f"$temp%1.1f")
            }
          }

          radioButtonFahrenheit.selected.onChange{
            tempType = 2
            var inputTemp = (inputTemperature.getText).toFloat
            var temp:Float = 0
            if(tempType ==1)
            {
              temp = (9/5.toFloat) * inputTemp + 32
              convertedTemperature.setText("Fahrenheit [°F] = " + f"$temp%1.1f")
            }
            else
            {
              temp = (inputTemp-32)/(9/5.toFloat)
              convertedTemperature.setText("Celsius [°C] = " + f"$temp%1.1f")
            }
          }
        }

        new BorderPane {
          center = centerBox
        }
      }
    }
  }
}
