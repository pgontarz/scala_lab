import scala.math._
object main {

  var collection = List[Int]()

  def main(args: Array[String]): Unit = {
    for(i <- 100 to 999){
      collection = collection:+i
    }

    val solves =
      for (number <- collection if ((number/100)%10)*100 + ((number/10)%10)*10 + (number%10) ==
        pow((number/100)%10,3) + pow((number/10)%10,3) + pow(number%10,3))  yield number

    println(solves)
  }
}

