object bmwCar {
  var odometer = 50000
  var HP = 300
  var color = "Red"

  def changeColor(color:String): Unit ={
    this.color = color
  }

  def showHP(): Unit ={
    println("Horse power: " + this.HP)
  }

  def showOdometer(): Unit ={
    println("Car's odometer is: " + this.odometer)
  }
}
