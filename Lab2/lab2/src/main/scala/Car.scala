class Car(var carType:String = "volvo", var HP:Int = 100, var odometer: Long = 100000) {

  def showName(): Unit ={
    println("This car is: " + this.carType)
  }

  def showHP(): Unit ={
    println("Horse power: " + this.HP)
  }

  def showOdometer(): Unit ={
    println("Car's odometer is: " + this.odometer)
  }

  def changeColor(color:String): Unit ={
    println("Car color is " + color)
  }
}
