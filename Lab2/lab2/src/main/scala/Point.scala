class Point(private var x: Int = 1,private var y: Int = 1) {

  protected def showCoords(): Unit ={
    println("Point (x: " + this.x + ", y:" + this.y + ")")
  }

  protected def getX(): Int = {
    this.x
  }

  protected def getY(): Int = {
    this.x
  }

  private def privatePointMethod(): Unit ={
    println("privatePointMethod")
  }

  def publicPointMethod(): Unit ={
    println("publicPointMethod")
  }

  override def toString: String =
    s"($x, $y)"
}