package Characters
class DarkMagician(name: String = "Nameless magician",
                   health: Int,
                   strenght: Int,
                   mana: Int) extends Magician(name, health, strenght, mana) with FireBallSpell {
  override var range: Int = 2000
  override var power: Int = 50

  def specialAttack(): Unit ={
    println(name + " uses special attack!")
  }

}
