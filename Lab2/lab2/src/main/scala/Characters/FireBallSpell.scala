package Characters
trait FireBallSpell
{
  var range: Int
  var power: Int
  def useFireBall()
  {
    println("Using FireBall spell!") ;
  }
}
