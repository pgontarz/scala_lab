package Characters

class Magician(name: String = "Nameless magician", health: Int, strenght: Int, mana: Int) extends Character(health, strenght, mana) {

  def getName: String = {
    this.name
  }

  override def attack() = {
    println(this.name + " attacks!")
  }

  def useWand(): Unit = {
    println(this.name + " uses magic wand! ")
  }

  override def toString: String = {
    s"Name: $name = " + super.toString
  }
}
