package Characters

class Warrior(name: String = "Nameless warrior", health: Int, strenght: Int, mana: Int) extends Character(health, strenght, mana) {

  def getName: String={
    this.name
  }

  override def attack() = {
    println(this.name + " attacks!")
  }

  def useAxes(): Unit ={
    println(this.name + " attacks with an axes! ")
  }

  override def toString: String = {
    s"Name: $name = " + super.toString
  }

}
