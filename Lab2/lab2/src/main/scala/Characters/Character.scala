package Characters

abstract class  Character(protected var health: Int = 1, protected var strenght: Int = 1, protected var mana: Int = 1) {

  def attack(): Unit ={

  }

  def setHealth(newHealth:Int){
    this.health = newHealth
  }

  def setStrenght(newStrenght:Int){
    this.strenght = newStrenght
  }

  def setMana(newMana:Int){
    this.mana = newMana
  }

  def getHealth():Int ={
    this.health
  }

  def getStrenght():Int ={
    this.strenght
  }

  def getMana():Int ={
    this.mana
  }

  override def toString: String = {
    s"(Health: $health, Strenght: $strenght, Mana: $mana)"
  }

}
