package Characters
class DarkWarrior(name: String = "Nameless magician",
                  health: Int,
                  strenght: Int,
                  mana: Int) extends Warrior(name, health, strenght, mana) with IronSkin {
  override var duration: Int = 5000
  override var abilityStrenght: Int = 20

  def specialAttack(): Unit ={
    println(name + " uses special attack!")
  }
}
