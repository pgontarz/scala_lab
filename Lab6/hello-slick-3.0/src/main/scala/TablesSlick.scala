import slick.driver.H2Driver.api._
import slick.lifted.{ProvenShape, ForeignKeyQuery}

class Osoby(tag: Tag) extends Table[(Int, String, String, String, Int)](tag, "osoby"){
    
    def id: Rep[Int] = column[Int]("OSOBA_ID", O.PrimaryKey, O.AutoInc)
    def imie: Rep[String] = column[String]("OSOBA_IMIE")
    def nazwisko: Rep[String] = column[String]("OSOBA_NAZWISKO")
    def dataUrodzenia: Rep[String] = column[String]("OSOBA_DATA_URODZENIA")
    def wiek: Rep[Int] = column[Int]("OSOBA_WIEK")
    
    def * : ProvenShape[(Int, String, String, String, Int)] =
        (id, imie, nazwisko, dataUrodzenia, wiek)
}

class Kontakty(tag: Tag) extends Table[(Int, Int, String, String)](tag, "kontakty"){
    
    def id: Rep[Int] = column[Int]("KONTAKT_ID", O.PrimaryKey, O.AutoInc)
    def osobaID: Rep[Int] = column[Int]("OSOBA_ID")
    def komunikacja: Rep[String] = column[String]("KONTAKT_KOMUNIKACJA")
    def kontakt: Rep[String] = column[String]("KONTAKT_WARTOSC")
    
    def * : ProvenShape[(Int, Int, String, String)] =
        (id, osobaID, komunikacja, kontakt)
        
    def osoba = foreignKey("OSOBA_FK", osobaID, TableQuery[Osoby])(_.id, 
    onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)
}