import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import slick.backend.DatabasePublisher
import slick.driver.H2Driver.api._

// The main application
object Lab6 extends App {
  val db = Database.forConfig("h2mem1")
  try {
    //tablica OSOBY
    val osoby: TableQuery[Osoby] = TableQuery[Osoby]
    //tablica KONTAKTY
    val kontakty: TableQuery[Kontakty] = TableQuery[Kontakty]
    val setupAction: DBIO[Unit] = DBIO.seq(
        
      (osoby.schema ++ kontakty.schema).create,
      osoby += (0, "Piotr", "Nowak", "12.05.2000", 23),
      osoby += (0, "Magda", "Kowalczyk", "22.10.1997", 26),
      osoby += (0, "Ryszard", "Wolik", "11.04.2000", 23),
      osoby += (0, "Marta", "Kaczan", "15.10.1997", 26),
      osoby += (0, "Karol", "Kosak", "19.05.2001", 22),
      osoby += (0, "Dariusz", "Szybki", "05.10.1997", 26),
      
      //PIOTR NOWAK
      kontakty += (0, 1, "email", "piotr@gmail.com"),
      kontakty += (0, 1, "telefon", "796854163"),
      //MAGDA KOWALCZYK
      kontakty += (0, 2, "email", "magda@gmail.com"),
      kontakty += (0, 2, "telefon", "792857163"),
      //RYSZARD WOLIK
      kontakty += (0, 3, "email", "ryszard@gmail.com"),
      kontakty += (0, 3, "telefon", "696356123"),
      //MARTA KACZAN
      kontakty += (0, 4, "email", "marta@gmail.com"),
      kontakty += (0, 4, "telefon", "796257123"),
      //KAROL KOSAK
      kontakty += (0, 5, "email", "karol@gmail.com"),
      kontakty += (0, 5, "telefon", "727874123"),
      //DARIUSZ SZYBKI
      kontakty += (0, 6, "email", "dariusz@gmail.com"),
      kontakty += (0, 6, "telefon", "627873213")
      
    )
    val setupFuture: Future[Unit] = db.run(setupAction)
    val f = setupFuture.flatMap { _ =>
      //<=================================================================  
      val allOsobyAction: DBIO[Seq[(Int, String, String, String, Int)]] =
        osoby.result
        
      val combinedOsobyAction: DBIO[Seq[(Int, String, String, String, Int)]] =
        allOsobyAction

      val combinedOsobyFuture: Future[Seq[(Int, String, String, String, Int)]] =
        db.run(combinedOsobyAction)

      combinedOsobyFuture.map { allOsoby =>
        //allOsoby.foreach(println)
      }
      //<=================================================================
      val allKontaktyAction: DBIO[Seq[(Int, Int, String, String)]] =
        kontakty.result
        
      val combinedKontaktyAction: DBIO[Seq[(Int, Int, String, String)]] =
        allKontaktyAction

      val combinedKontaktyFuture: Future[Seq[(Int, Int, String, String)]] =
        db.run(combinedKontaktyAction)

      combinedKontaktyFuture.map { allKontakty =>
        //allKontakty.foreach(println)
      }
      //<=================================================================  
    }.flatMap { _ =>
    // a) REKORDY OSOB KTORYCH IMIE ZACZYNA SIE NA LITERE ALFABETU
      val filterQuery = osoby.filter(x => x.imie.startsWith("M"))
      db.run(filterQuery.result.map(println))

    }.flatMap { _ =>
    // b) POLICZYC DOSTEPNE W BAZIE KONTAKTY JEDNEGO RODZAJU
      val filterQuery = kontakty.filter(x => x.komunikacja === "email")
      db.run(filterQuery.result.map(
          res => println(res + "\n Ilosc kontaktow szukanego rodzjau: " + res.length)  ))

    }.flatMap { _ =>
    // c) WYBRAC Z DANYCH WSZYSTKIE KONTAKTY DLA WYBRANEJ OSOBY
      val posts = for {
        c <- kontakty 
        p <- c.osoba if p.imie === "Ryszard"
          
      } yield (c, p)

      db.run(posts.result).map(println)
    }
    .flatMap { _ =>
    // d) WYSZUKAC W ZESTAWIENIU KONTAKTOW ZADANY CIAG ZNAKOW ORAZ OKRESLIC OSOBE LUB OSOBY ZWIAZANE Z DANYM KONTAKTEM
        val posts = for {
          c <- kontakty  if c.kontakt like "%123%"
          p <- c.osoba 
          
        } yield (c, p)

      db.run(posts.result).map(println)
    }
    //<================================================================= 
    Await.result(f, Duration.Inf)
  } finally db.close
}
