package models
import collection.mutable

case class LoginData(username: String, password: String)

case class UserData(var id: Int, var firstname: String, var surname: String, var age: Int, var company: String, var phone: String, var emailAddress: String )

object ManagementModel {
  //stare
  private val users = mutable.Map[String,String]("jan@onet.pl" -> "pass", "tomek@onet.pl" -> "pass12")

  //<============================= USER DATA =============================
  //data for testing
  private val userData = mutable.Map[String, List[UserData]]("jan@onet.pl" -> List[UserData](
    UserData(1, "Jan", "Kowalski", 22, "Nokia", "+48796937822", "jan@onet.pl"),
    UserData(2, "Karol", "Kasperski", 32, "Samsung", "+48796937822", "karol@onet.pl"),
    UserData(3,"Ola", "Lojalna", 26, "Nokia", "+48796937822", "ola@onet.pl"),
    UserData(4, "Marta", "Kowalska", 22, "Aptiv", "+48796937822", "marta@onet.pl"),
    UserData(5, "Franek", "Szybki", 32, "Samsung", "+48796937822", "franek@onet.pl"),
    UserData(6,"Ziemowit", "Lojalny", 26, "Nokia", "+48796937822", "Ziemowit@onet.pl"),
    UserData(7,"Kamil", "Frytka", 26, "SmartTech", "+48796937822", "arek@onet.pl"),
    UserData(8,"Witek", "Maczeta", 33, "Warsztat", "+48796937822", "witek@onet.pl"),
    UserData(9,"Ania", "Okon", 26, "AGH", "+48796937822", "ania@onet.pl"),
    UserData(10,"Marek", "Zacny", 53, "Sklep komputerowy", "+48796937822", "marek@onet.pl")),
  )

  //<============================= FUNCTIONS =============================
  def getRootUserData(username: String): List[UserData] = {
    userData.get(username).getOrElse(Nil)
  }

  def removeUserData(username: String, index: Int): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username) = userData(username).patch(index, Nil, 1)
      true
    }
  }

  def addNewContact(username: String, newContact: UserData): Unit = {
    userData(username) = newContact :: userData.get(username).getOrElse(Nil)
  }

  def updateUserFirstname(username: String, index: Int, newFirstname: String): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).firstname = newFirstname
      true
    }
  }

  def updateUserSurname(username: String, index: Int, newSurname: String): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).surname = newSurname
      true
    }
  }

  def updateUserAge(username: String, index: Int, newAge: Int): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).age = newAge
      true
    }
  }

  def updateUserCompany(username: String, index: Int, newCompany: String): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).company = newCompany
      true
    }
  }

  def updateUserPhone(username: String, index: Int, newPhone: String): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).phone = newPhone
      true
    }
  }

  def updateUserEmail(username: String, index: Int, newEmail: String): Boolean = {
    if(index < 0  || userData.get(username).isEmpty || index >= userData(username).length) false
    else{
      userData(username)(index).emailAddress = newEmail
      true
    }
  }


  //<===============================LOGIN======================================

  def validateUser(username: String, password: String): Boolean = {
    users.get(username).map(_ == password).getOrElse(false)
  }

  def createUser(username: String, password: String): Boolean = {
    if(users.contains(username)) false else {
      users(username) = password
      true
    }
  }
}