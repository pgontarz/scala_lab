package controllers

import javax.inject._
import play.api.mvc._
import javax.inject.Singleton
import models.{ManagementModel, UserData}

@Singleton
class DataController @Inject()(val cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  def contactsPage = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map{username =>
      val data = ManagementModel.getRootUserData(username)
      Ok(views.html.contactsPage(data))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def deleteUserData = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val index = args("index").head.toInt
        ManagementModel.removeUserData(username,index)
        Redirect(routes.DataController.contactsPage())
      }.getOrElse(Redirect(routes.DataController.contactsPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserDataPage = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val index = args("index").head.toInt
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.contactsPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserFirstname = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("firstname").head
        val index = args("index").head.toInt
        ManagementModel.updateUserFirstname(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserSurname = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("surname").head
        val index = args("index").head.toInt
        ManagementModel.updateUserSurname(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserAge = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("age").head.toInt
        val index = args("index").head.toInt
        ManagementModel.updateUserAge(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserCompany = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("company").head
        val index = args("index").head.toInt
        ManagementModel.updateUserCompany(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserPhone = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("phone").head
        val index = args("index").head.toInt
        ManagementModel.updateUserPhone(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def updateUserEmail = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        val editData = args("emailAddress").head
        val index = args("index").head.toInt
        ManagementModel.updateUserEmail(username,index,editData)
        Ok(views.html.editUser(ManagementModel.getRootUserData(username)(index),index))
      }.getOrElse(Redirect(routes.DataController.updateUserDataPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }


  def addUserPage = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map{username =>
      Ok(views.html.addUser())
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

  def addNewContactData = Action{ implicit request =>
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map { args =>
        var id = 0
        if(ManagementModel.getRootUserData(username).isEmpty){
          id = 0
        }
        else {
          id = ManagementModel.getRootUserData(username).sortBy(_.id).last.id + 1
        }
        val firstname = args("firstname").head
        val surname = args("surname").head
        val age = args("age").head.toInt
        val company = args("company").head
        val phone = args("phone").head
        val email = args("emailAddress").head
        ManagementModel.addNewContact(username,UserData(id,firstname,surname,age,company,phone,email))
        Redirect(routes.DataController.contactsPage()).withSession("username"-> username).flashing("success" -> "Added new contact!")
      }.getOrElse(Redirect(routes.DataController.addUserPage()))
    }.getOrElse(Redirect(routes.LoginController.login()))
  }

}
