package controllers
import javax.inject._
import play.api.mvc._
import javax.inject.Singleton
import models.{LoginData, ManagementModel}
import play.api.data.Form
import play.api.data.Forms._

@Singleton
class LoginController @Inject()(val cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  val loginForm = Form(mapping(
    "Username" -> email,
    "Password" -> text(3,10)
     )(LoginData.apply)(LoginData.unapply))

  def login = Action{ implicit request =>
    Ok(views.html.login(loginForm))
  }

  def validateLoginForm = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithError => BadRequest(views.html.login(formWithError)),
      loginData =>
          if(ManagementModel.validateUser(loginData.username,loginData.password)){
            Redirect(routes.DataController.contactsPage()).withSession("username"-> loginData.username).flashing("success" -> "Login success!")
          }
          else{
            Redirect(routes.LoginController.login()).flashing("error" -> "Invalid username/password combination!")
          }
    )
  }

  def createUser = Action{ implicit  request =>
    loginForm.bindFromRequest.fold(
      formWithError => BadRequest(views.html.login(formWithError)),
      loginData =>
        if(ManagementModel.createUser(loginData.username,loginData.password)){
          Redirect(routes.DataController.contactsPage()).withSession("username"-> loginData.username).flashing("success" -> "Creating new user success!")
        }
        else{
          Redirect(routes.LoginController.login()).flashing("error" -> "Invalid username/password combination!")
        }
    )
  }

  def logout = Action{
    Redirect(routes.LoginController.login()).withNewSession
  }

}
