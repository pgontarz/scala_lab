import scala.math._

object main {

  def main(args: Array[String]){

    println("Task 1")
    drawRect(8,3)
    println("---------------------")
    println("Task 2")
    println(calculate())
    println("---------------------")
    println("Task 3")
    findResults()
  }


  def drawRect(a: Int, b:Int){
    for( x <- 1 to b)
    {
      for( y <- 1 to a)
      {
        print(" * ");
      }
      println();
    }
  }

  def calculate():Double = {
    var fraction = (1.toDouble/3.toDouble)
    var volume = 2*log(521) + (7.5e8)/(128*422).toDouble
    var result = pow(volume, fraction)
    return result
  }

  def findResults(){
    for( a <- 1 to 9)
    {
      for( b <- 0 to 9)
      {
        for( c <- 0 to 9)
        {
          if ((100*a + 10*b + c) == (pow(a,3)+pow(b,3)+pow(c,3))) {
            var result = pow(a,3)+pow(b,3)+pow(c,3)
            var number = 100*a + 10*b + c
            println("Znaleziona liczba: " + number + ", której suma sześcianów poszczególnych cyfr jest równa: " + result)
          }
        }
      }
    }
  }

}

